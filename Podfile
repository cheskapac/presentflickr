platform :ios, '11.0'
use_frameworks!
inhibit_all_warnings!

def tests_pods
    # RxSwift Testing extensions
    pod 'RxTest', '~> 4.1.2'
    # RxSwift Blocking operatos
    pod 'RxBlocking', '~> 4.1.2'
end

target 'Domain' do
    # RxSwift is a Swift implementation of Reactive Extensions
    pod 'RxSwift', '~> 4.1.2'

    target 'DomainTests' do
        inherit! :search_paths
        tests_pods
    end
end

target 'NetworkPlatform' do
    # RxSwift is a Swift implementation of Reactive Extensions
    pod 'RxSwift', '~> 4.1.2'
    # Elegant HTTP Networking in Swift
    pod 'Alamofire', '~> 4.7.1'
    # RxSwift wrapper around the elegant HTTP networking in Swift Alamofire
    pod 'RxAlamofire', '~> 4.2.0'
    # JSON Object mapping written in Swift
    pod 'ObjectMapper', '~> 3.1.0'
    # An extension to Alamofire which automatically converts JSON response data into swift objects using ObjectMapper
    pod 'AlamofireObjectMapper', '~> 5.0.0'
    # Asynchronous image downloader with cache support with an UIImageView category.
    pod 'SDWebImage', '~> 4.3.3'
    # An easy way to use pull-to-refresh
    pod 'MJRefresh', '~> 3.1.15.3'
    # A turnkey solution to display photos and images of all kinds in your app.
    pod 'BFRImageViewer', '~> 1.1.3'
    
    target 'NetworkPlatformTests' do
        inherit! :search_paths
        tests_pods
    end
end

target 'PresentConnectionHomework' do
    # RxSwift is a Swift implementation of Reactive Extensions
    pod 'RxSwift', '~> 4.1.2'
    # RxSwift Cocoa extensions
    pod 'RxCocoa', '~> 4.1.2'
    # Best and lightest-weight crash reporting for mobile, desktop and tvOS.
    pod 'Fabric'
    pod 'Crashlytics'

    target 'PresentConnectionHomeworkTests' do
        inherit! :search_paths
        tests_pods
    end
end
