//
//  Application.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain
import NetworkPlatform

final class Application {
    static let shared = Application()
    
    private let networkUseCaseProvider: Domain.UseCaseProvider
    
    private init() {
        networkUseCaseProvider = NetworkPlatform.UseCaseProvider()
    }
    
    func configureInitialInterface(in window: UIWindow) {
        let navigationController = UINavigationController()
        window.rootViewController = navigationController

        let storyboard = StoryboardScene.Photos.storyboard
        let photosNavigator = DefaultPhotosNavigator(
            services: networkUseCaseProvider,
            navigationController: navigationController,
            storyBoard: storyboard)
        photosNavigator.navigateToPhotoList()
    }
}
