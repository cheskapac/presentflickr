//
//  Constants.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

enum Constants {
    enum Search {
        static let throttleTimeInterval = 0.3
        static let minCharacterCount = 3
    }
}
