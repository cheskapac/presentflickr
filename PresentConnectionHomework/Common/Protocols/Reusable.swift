//
//  Reusable.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

protocol Reusable {
    static var reuseIdentifier: String { get }
}
