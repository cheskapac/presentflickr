//
//  UICollectionViewCell+Reusable.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit

extension UICollectionViewCell: Reusable {
    static var reuseIdentifier: String {
        return classNameFromClass(self)
    }
}
