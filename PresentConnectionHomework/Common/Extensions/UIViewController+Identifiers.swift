//
//  UIViewController+Identifiers.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit

extension UIViewController {
    static var identifier: String {
        return classNameFromClass(self)
    }
}
