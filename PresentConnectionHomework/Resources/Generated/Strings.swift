// Generated using SwiftGen, by O.Halligon — https://github.com/SwiftGen/SwiftGen

import Foundation

// swiftlint:disable superfluous_disable_command
// swiftlint:disable file_length

// swiftlint:disable explicit_type_interface identifier_name line_length nesting type_body_length type_name
enum L10n {

  enum Photos {

    enum Photolist {

      enum Zerostate {

        enum Message {
          /// 🤕\nSomething went wrong\nPlease check your spelling\notherwise internet connectivity
          static let emptySearchResult = L10n.tr("Localizable", "Photos.PhotoList.ZeroState.Message.EmptySearchResult")
          /// 🕵️‍♂️\nSearching...
          static let fetching = L10n.tr("Localizable", "Photos.PhotoList.ZeroState.Message.Fetching")
          /// ✍️\nEnter search phrase\n[ >%d characters ]
          static func shortSearchPhrase(_ p1: Int) -> String {
            return L10n.tr("Localizable", "Photos.PhotoList.ZeroState.Message.ShortSearchPhrase", p1)
          }
        }
      }
    }
  }
}
// swiftlint:enable explicit_type_interface identifier_name line_length nesting type_body_length type_name

extension L10n {
  private static func tr(_ table: String, _ key: String, _ args: CVarArg...) -> String {
    let format = NSLocalizedString(key, tableName: table, bundle: Bundle(for: BundleToken.self), comment: "")
    return String(format: format, locale: Locale.current, arguments: args)
  }
}

private final class BundleToken {}
