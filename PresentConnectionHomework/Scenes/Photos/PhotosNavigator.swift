//
//  PhotosNavigator.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit
import Domain

protocol PhotosNavigator {
    func navigateToPhotoList()
}

class DefaultPhotosNavigator: PhotosNavigator {
    private let storyBoard: UIStoryboard
    private let navigationController: UINavigationController
    private let services: UseCaseProvider
    
    init(services: UseCaseProvider,
         navigationController: UINavigationController,
         storyBoard: UIStoryboard) {
        self.services = services
        self.navigationController = navigationController
        self.storyBoard = storyBoard
    }
    
    func navigateToPhotoList() {
        let viewController: PhotoListViewController = storyBoard.instantiateViewController()
        viewController.viewModel = PhotoListViewModel(
            useCase: services.makePhotosUseCase(),
            navigator: self)
        navigationController.setNavigationBarHidden(true, animated: false)
        navigationController.pushViewController(viewController, animated: true)
    }
}
