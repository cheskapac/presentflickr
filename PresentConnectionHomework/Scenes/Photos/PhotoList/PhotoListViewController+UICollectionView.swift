//
//  PhotoListViewController+UICollectionView.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit

extension PhotoListViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
            return PhotoListCell.cellSize(for: collectionView, at: indexPath)
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
            return PhotoListCell.spacing(for: collectionView, forSectionAt: section)
    }
}
