//
//  PhotoListViewController.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit
import Domain
import RxSwift
import RxCocoa
import MJRefresh
import BFRImageViewer

class PhotoListViewController: UIViewController {

    private let disposeBag = DisposeBag()
    
    var viewModel: PhotoListViewModel!
    
    // MARK: - Outlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private weak var emptyStateLabel: UILabel!

    // MARK: - Methods -
    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }
    
    // MARK: - Configure
    private func configure() {
        guard viewModel != nil else {
            fatalError("View model is not set")
        }

        configureCollectionView()
        bindOutputToCollectionView(with: input)
    }
    
    // MARK: - Helpers
    private var triggerSearchDriver: Driver<Void> {
        let searchBarTextChanged = searchBar.rx
            .text
            .asDriver()
            .mapToVoid()
        let loadMoreCalled = rx.sentMessage(#selector(loadMore))
            .mapToVoid()
            .asDriverOnErrorJustComplete()
        return Driver.merge(searchBarTextChanged, loadMoreCalled)
    }
    
    private var input: PhotoListViewModel.Input {
        let textDriver = searchBar.rx.text.orEmpty.asDriver()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        return input
    }
    
    @objc
    private func loadMore() {
        print("load more items")
    }
    
    // MARK: - Configuration
    private func configureCollectionView() {
        registerCells()
        collectionView.delegate = self

        collectionView.mj_footer = MJRefreshAutoNormalFooter(
            refreshingTarget: self,
            refreshingAction: #selector(loadMore))
        collectionView.mj_footer.isHidden = true
    }
    
    private func registerCells() {
        let photoListCellNib = UINib(nibName: PhotoListCell.reuseIdentifier, bundle: nil)
        collectionView.register(photoListCellNib, forCellWithReuseIdentifier: PhotoListCell.reuseIdentifier)
    }
    
    // MARK: - RX Binding
    private func bindOutputToCollectionView(with input: PhotoListViewModel.Input) {
        let output = viewModel.transform(input: input)
        output.cellViewModelList
            .drive(collectionView.rx.items(
                cellIdentifier: PhotoListCell.reuseIdentifier,
                cellType: PhotoListCell.self)) { _, viewModel, cell in
                    cell.bind(viewModel)
            }
            .disposed(by: disposeBag)
        
        bindCollectionViewRefreshFooter(with: output)
        bindCellSelection(with: output)
        bindEmptyState(with: output)
    }
    
    private func bindCollectionViewRefreshFooter(with output: PhotoListViewModel.Output) {
        output.cellViewModelList
            .drive(onNext: { [unowned self] (photoList) in
                self.collectionView.mj_footer.isHidden = photoList.isEmpty
            }).disposed(by: disposeBag)
        
        output.isFetching
            .drive(onNext: { [unowned self] (isFetching) in
                if !isFetching {
                    self.collectionView.mj_footer.endRefreshing()
                }
            })
            .disposed(by: disposeBag)
    }
    
    private func bindCellSelection(with output: PhotoListViewModel.Output) {
        let photoListItemViewModels = BehaviorRelay<[PhotoListItemViewModel]>(value: [])
        output.cellViewModelList
            .asObservable()
            .bind(to: photoListItemViewModels)
            .disposed(by: disposeBag)
        
        collectionView.rx.itemSelected
            .asDriver()
            .map({ (indexPath) -> (UIImage?, Photo) in
                let photoListItemViewModels = photoListItemViewModels.value
                let photo = photoListItemViewModels[indexPath.item].photo
                guard let cell = self.collectionView.cellForItem(at: indexPath) as? PhotoListCell,
                    let cellImageView = cell.imageView,
                    let cellImage = cellImageView.image else {
                        return (nil, photo)
                }
                return (cellImage, photo)
            }).drive(onNext: { [unowned self] (image, photo) in
                self.openFullscreenImage(image, for: photo)
            }).disposed(by: disposeBag)
    }
    
    private func bindEmptyState(with output: PhotoListViewModel.Output) {
        output.cellViewModelList
            .asObservable()
            .map { (cellViewModelList) -> Bool in
                return !cellViewModelList.isEmpty
            }
            .bind(to: emptyStateLabel.rx.isHidden)
            .disposed(by: disposeBag)

        Driver.combineLatest(searchBar.rx.text.orEmpty.asDriver(), output.isFetching)
            .asObservable()
            .map { (text, isFetching) -> String in
                let minCharactersCount = Constants.Search.minCharacterCount
                if text.count < minCharactersCount {
                    return L10n.Photos.Photolist.Zerostate.Message.shortSearchPhrase(minCharactersCount - 1)
                } else if isFetching {
                    return L10n.Photos.Photolist.Zerostate.Message.fetching
                } else {
                    return L10n.Photos.Photolist.Zerostate.Message.emptySearchResult
                }
            }
            .bind(to: emptyStateLabel.rx.text)
            .disposed(by: disposeBag)
    }
}

// MARK: - Navigation
extension PhotoListViewController {
    private func openFullscreenImage(_ image: UIImage?, for photo: Photo) {
        guard let photoUrl = photo.url(size: .medium640) else {
            return
        }

        let bfrImageViewController: BFRImageViewController?
        if let initialImage = image,
            let backloadedImage = BFRBackLoadedImageSource(initialImage: initialImage, hiResURL: photoUrl) {
            bfrImageViewController = BFRImageViewController(imageSource: [backloadedImage])
        } else {
            bfrImageViewController = BFRImageViewController(imageSource: [photoUrl])
        }

        guard let imageViewController = bfrImageViewController else {
            return
        }
        present(imageViewController, animated: true)
    }
}
