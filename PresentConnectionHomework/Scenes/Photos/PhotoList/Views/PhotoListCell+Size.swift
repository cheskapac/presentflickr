//
//  PhotoListCell+Size.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit

extension PhotoListCell {
    private static let defaultHeight: CGFloat = 240.0
    private static let defaultSpacing: CGFloat = 8.0
    
    static func cellSize(for collectionView: UICollectionView, at indexPath: IndexPath) -> CGSize {
        var width = collectionView.bounds.size.width
        if !isWideCell(at: indexPath) {
            width = (width - defaultSpacing) / 2
        }
        return CGSize(width: width, height: defaultHeight)
    }
    
    static func spacing(for collectionView: UICollectionView, forSectionAt section: Int) -> CGFloat {
        return defaultSpacing
    }
    
    // MARK: - Helpers
    private static func isWideCell(at indexPath: IndexPath) -> Bool {
        return indexPath.row % 3 == 2
    }
}
