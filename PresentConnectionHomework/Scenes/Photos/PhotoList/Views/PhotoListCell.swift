//
//  PhotoListCell.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit
import SDWebImage

final class PhotoListCell: UICollectionViewCell {
    @IBOutlet private weak var titleContainer: UIView!
    @IBOutlet private weak var titleLabel: UILabel!
    @IBOutlet private(set) weak var imageView: UIImageView!
    
    func bind(_ viewModel: PhotoListItemViewModel) {
        titleContainer.isHidden = viewModel.shouldHideTitle
        titleLabel.text = viewModel.title
        imageView.sd_addActivityIndicator()
        imageView.sd_setImage(with: viewModel.smallImageURL)
    }
}
