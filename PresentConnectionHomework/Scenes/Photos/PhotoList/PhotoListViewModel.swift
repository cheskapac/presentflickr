//
//  PhotoListViewModel.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain
import RxSwift
import RxCocoa

final class PhotoListViewModel: ViewModelType {
    struct Input {
        let text: Driver<String>
        let triggerSearch: Driver<Void>
    }
    struct Output {
        let isFetching: Driver<Bool>
        let cellViewModelList: Driver<[PhotoListItemViewModel]>
        let error: Driver<Error>
    }
    
    private let useCase: PhotosUseCase
    private let navigator: PhotosNavigator
    
    private var page = 1
    private var searchText = ""
    private var maxUploadDate: TimeInterval = 0
    private var cellViewModelList = [PhotoListItemViewModel]()
    private var cellViewModelListResult = BehaviorRelay<[PhotoListItemViewModel]>(value: [])
    private let disposeBag = DisposeBag()

    private var canTriggerSearch: Bool {
        return self.searchText.count >= Constants.Search.minCharacterCount
    }

    init(useCase: PhotosUseCase, navigator: PhotosNavigator) {
        self.useCase = useCase
        self.navigator = navigator
    }
    
    func transform(input: Input) -> Output {
        updateSearchParameters(with: input)

        let activityIndicator = ActivityIndicator()
        let errorTracker = ErrorTracker()
        executeSearch(with: input, activityIndicator: activityIndicator, errorTracker: errorTracker)
            .drive(onNext: { [unowned self] (cellViewModelList) in
                self.cellViewModelList.append(contentsOf: cellViewModelList)
                self.cellViewModelListResult.accept(self.cellViewModelList)
            }).disposed(by: disposeBag)
        
        let isFetchingDriver = activityIndicator.asDriver()
        let errorDriver = errorTracker.asDriver()
        return Output(
            isFetching: isFetchingDriver,
            cellViewModelList: cellViewModelListResult.asDriver(),
            error: errorDriver)
    }
    
    // MARK: - Helpers
    private func updateSearchParameters(with input: Input) {
        input.text.drive(onNext: { [unowned self] (text) in
            if self.shouldResetSearchParameters(withText: text) {
                self.resetSearchParameters()
            }
            self.searchText = text
        }).disposed(by: disposeBag)

        input.triggerSearch.asDriver().drive(onNext: { [unowned self] (_) in
            guard self.canTriggerSearch else {
                return
            }
            self.page += 1
        }).disposed(by: disposeBag)
    }
    
    private func shouldResetSearchParameters(withText text: String) -> Bool {
        return text != self.searchText
    }
    
    private func resetSearchParameters() {
        self.page = 0
        self.maxUploadDate = Date().timeIntervalSince1970
        self.cellViewModelList.removeAll()
        self.cellViewModelListResult.accept([])
    }
    
    private func executeSearch(
        with input: Input,
        activityIndicator: ActivityIndicator,
        errorTracker: ErrorTracker) -> Driver<[PhotoListItemViewModel]> {
            let photos = input.triggerSearch.flatMapLatest { [unowned self] _ -> Driver<[PhotoListItemViewModel]> in
                guard self.canTriggerSearch else {
                    return Driver<[PhotoListItemViewModel]>.empty()
                }
                
                return self.useCase.search(
                    text: self.searchText,
                    maxUploadDate: self.maxUploadDate,
                    page: self.page)
                    .trackActivity(activityIndicator)
                    .trackError(errorTracker)
                    .asDriverOnErrorJustComplete()
                    .map({ (searchStatus) -> [PhotoListItemViewModel] in
                        return searchStatus.searchDetails?.photoList.map { PhotoListItemViewModel(photo: $0) } ?? []
                    })
            }
            return photos
    }
    
}
