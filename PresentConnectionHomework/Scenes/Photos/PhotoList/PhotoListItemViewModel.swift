//
//  PhotoListItemViewModel.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain

final class PhotoListItemViewModel {
    let photo: Photo
    let title: String
    var smallImageURL: URL?
    
    var shouldHideTitle: Bool {
        return title.isEmpty
    }
    
    init(photo: Photo) {
        self.photo = photo
        title = photo.title ?? ""
        smallImageURL = photo.url(size: .small320)
    }
}
