//
//  AppDelegate.swift
//  PresentConnectionHomework
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import UIKit
import Fabric
import Crashlytics

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?

    func application(
        _ application: UIApplication,
        didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
            Fabric.with([Crashlytics.self])

            let window = UIWindow(frame: UIScreen.main.bounds)
            Application.shared.configureInitialInterface(in: window)
            self.window = window
            return true
    }
}
