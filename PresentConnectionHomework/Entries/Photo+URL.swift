//
//  Photo+URL.swift
//  Domain
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain

extension Photo {
    public func url(size: PhotoSize) -> URL? {
        // https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}_[mstzb].jpg
        let url = URL(string: "https://farm\(farm).staticflickr.com/\(server)/\(id)_\(secret)\(size.rawValue).jpg")
        return url
    }
}
