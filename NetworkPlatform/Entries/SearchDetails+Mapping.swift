//
//  Photos+Mapping.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Domain
import ObjectMapper

extension SearchDetails: ImmutableMappable {
    public init(map: Map) throws {
        self.init(
            page: try map.value("page"),
            totalPages: try map.value("pages"),
            itemsPerPage: try map.value("perpage"),
            totalItems: Int(try map.value("total")) ?? 0,
            photoList: try map.value("photo"))
    }
}
