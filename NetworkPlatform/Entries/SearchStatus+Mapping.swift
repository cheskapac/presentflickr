//
//  SearchStatus+Mapping.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Domain
import ObjectMapper

extension SearchStatus: ImmutableMappable {
    public init(map: Map) throws {
        self.init(
            status: try map.value("stat"),
            code: try? map.value("code"),
            message: try? map.value("message"),
            searchDetails: try? map.value("photos"))
    }
}
