//
//  Photo+Mapping.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Domain
import ObjectMapper

extension Photo: ImmutableMappable {
    public init(map: Map) throws {
        self.init(
            id: try map.value("id"),
            owner: try map.value("owner"),
            secret: try map.value("secret"),
            server: try map.value("server"),
            farm: try map.value("farm"),
            title: try? map.value("title"),
            isPublic: try map.value("ispublic"),
            isFriend: try map.value("isfriend"),
            isFamily: try map.value("isfamily"))
    }
}
