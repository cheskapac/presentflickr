//
//  Constants.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

enum Constants {
    enum Flickr {
        static let apiEndpoint = "https://api.flickr.com/services/rest/"
        static let apiKey = "05af1d89c61ad0613d1bdd133edd33a4"
        static let apiSecret = "014985de29578aee"
    }
    
    enum ItemsPerPage {
        static let defaultCount = 15
    }
}
