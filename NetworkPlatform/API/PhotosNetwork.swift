//
//  PhotosNetwork.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Domain
import RxSwift

public final class PhotosNetwork {
    private let network: Network<SearchStatus>
    
    init(network: Network<SearchStatus>) {
        self.network = network
    }
    
    public func searchPhotos(text: String, maxUploadDate: TimeInterval, page: Int) -> Observable<SearchStatus> {
        let parameters = [
            "method": "flickr.photos.search",
            "api_key": "\(Constants.Flickr.apiKey)",
            "text": text,
            "max_upload_date": "\(maxUploadDate)",
            "per_page": "\(Constants.ItemsPerPage.defaultCount)",
            "page": "\(page < 1 ? 1 : page)",
            "format": "json",
            "nojsoncallback": "1"
        ]
        return network.getItem("", parameters: parameters)
    }
}
