//
//  Network.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Alamofire
import Domain
import RxAlamofire
import RxSwift
import ObjectMapper

final class Network<T: ImmutableMappable> {
    
    private let endPoint: String
    private let scheduler: ConcurrentDispatchQueueScheduler
    
    init(_ endPoint: String) {
        self.endPoint = endPoint
        let dispatchQos = DispatchQoS(
            qosClass: DispatchQoS.QoSClass.background,
            relativePriority: 1)
        self.scheduler = ConcurrentDispatchQueueScheduler(qos: dispatchQos)
    }
    
    func getItems(_ path: String, parameters: [String: String]) -> Observable<[T]> {
        let absolutePath = "\(endPoint)/\(path)"
        return RxAlamofire
            .json(.get,
                  absolutePath,
                  parameters: parameters,
                  encoding: URLEncoding.queryString)
            .debug()
            .observeOn(scheduler)
            .map({ json -> [T] in
                return try Mapper<T>().mapArray(JSONObject: json)
            })
    }
    
    func getItem(_ path: String, parameters: [String: String]) -> Observable<T> {
        let absolutePath = "\(endPoint)/\(path)"
        return RxAlamofire
            .json(.get,
                  absolutePath,
                  parameters: parameters,
                  encoding: URLEncoding.queryString)
            .debug()
            .observeOn(scheduler)
            .map({ json -> T in
                return try Mapper<T>().map(JSONObject: json)
            })
    }
}
