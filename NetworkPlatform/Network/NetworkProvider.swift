//
//  NetworkProvider.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Domain

final class NetworkProvider {
    private let apiEndpoint: String
    
    public init() {
        apiEndpoint = Constants.Flickr.apiEndpoint
    }
    
    public func makePhotosNetwork() -> PhotosNetwork {
        let network = Network<SearchStatus>(apiEndpoint)
        return PhotosNetwork(network: network)
    }
}
