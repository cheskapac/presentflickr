//
//  PhotosUseCase.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain
import RxSwift

final class PhotosUseCase: Domain.PhotosUseCase {
    private let network: PhotosNetwork
    
    init(network: PhotosNetwork) {
        self.network = network
    }
    
    func search(text: String, maxUploadDate: TimeInterval, page: Int) -> Observable<SearchStatus> {
        let photos = network.searchPhotos(text: text, maxUploadDate: maxUploadDate, page: page)
        return photos
    }
}

struct MapFromNever: Error {}
extension ObservableType where E == Never {
    func map<T>(to: T.Type) -> Observable<T> {
        return self.flatMap { _ in
            return Observable<T>.error(MapFromNever())
        }
    }
}
