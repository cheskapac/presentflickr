//
//  UseCaseProvider.swift
//  NetworkPlatform
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import Domain

public final class UseCaseProvider: Domain.UseCaseProvider {
    private let networkProvider: NetworkProvider
    
    public init() {
        networkProvider = NetworkProvider()
    }
    
    public func makePhotosUseCase() -> Domain.PhotosUseCase {
        let network = networkProvider.makePhotosNetwork()
        return PhotosUseCase(network: network)
    }
}
