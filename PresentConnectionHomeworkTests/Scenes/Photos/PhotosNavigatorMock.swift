//
//  PhotosNavigatorMock.swift
//  PresentConnectionHomeworkTests
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

@testable import PresentConnectionHomework
import Domain

class PhotosNavigatorMock: PhotosNavigator {
    var navigateToPhotoList_called = false

    func navigateToPhotoList() {
        navigateToPhotoList_called = true
    }
}
