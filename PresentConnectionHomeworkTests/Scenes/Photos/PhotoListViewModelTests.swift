//
//  PhotoListViewModelTests.swift
//  PresentConnectionHomeworkTests
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

@testable import PresentConnectionHomework
import Domain
import XCTest
import RxSwift
import RxCocoa
import RxBlocking

enum TestError: Error {
    case test
}

class PhotoListViewModelTests: XCTestCase {
    var useCase: PhotosUseCaseMock!
    var navigator: PhotosNavigatorMock!
    var viewModel: PhotoListViewModel!
    
    let disposeBag = DisposeBag()
    
    override func setUp() {
        super.setUp()
        useCase = PhotosUseCaseMock()
        navigator = PhotosNavigatorMock()
        viewModel = PhotoListViewModel(useCase: useCase, navigator: navigator)
    }
    
    func test_transform_triggerSearch_doesNotExecuteSearch_whenTextIsToShort() {
        // setup
        let triggerSearchDriver = Observable<Void>.just(()).asDriverOnErrorJustComplete()
        let textDriver = BehaviorSubject<String>(value: "12").asDriverOnErrorJustComplete()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        let output = viewModel.transform(input: input)
        
        // test
        output.cellViewModelList.drive().disposed(by: disposeBag)
        
        // validate
        XCTAssertFalse(useCase.search_called)
    }
    
    func test_transform_triggerSearch_executeSearch_whenTextIsLongEnough() {
        // setup
        let triggerSearchDriver = Observable<Void>.just(()).asDriverOnErrorJustComplete()
        let textDriver = BehaviorSubject<String>(value: "123").asDriverOnErrorJustComplete()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        let output = viewModel.transform(input: input)
        
        // test
        output.cellViewModelList.drive().disposed(by: disposeBag)
        
        // validate
        XCTAssertTrue(useCase.search_called)
    }
    
    func test_transform_triggerSearch_trackIsFetching() {
        // setup
        let isFetchingValueListExpected = [true, false]
        let triggerSearchDriver = BehaviorSubject<Void>(value: ()).asDriverOnErrorJustComplete()
        let textDriver = BehaviorSubject<String>(value: "123").asDriverOnErrorJustComplete()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        let output = viewModel.transform(input: input)
        
        // test
        var isFetchingValueListActual = [Bool]()
        output.isFetching
            .do(onNext: { (isFetching) in
                isFetchingValueListActual.append(isFetching)
            }, onCompleted: {
                
            }, onSubscribe: {
                isFetchingValueListActual.append(true)
            }, onSubscribed: {
                
            }, onDispose: {
                
            })
            .drive()
            .disposed(by: disposeBag)
        
        // validate
        XCTAssertEqual(isFetchingValueListExpected, isFetchingValueListActual)
    }

    func test_transform_triggerSearch_mapPhotosToViewModel() {
        // setup
        useCase.search_returnValue = Observable<SearchStatus>.just(mockedSearchStatus())
        let triggerSearchDriver = Observable<Void>.just(()).asDriverOnErrorJustComplete()
        let textDriver = BehaviorSubject<String>(value: "123").asDriverOnErrorJustComplete()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        let output = viewModel.transform(input: input)
        
        // test
        output.cellViewModelList.drive().disposed(by: disposeBag)
        
        // validate
        let cellViewModelList = try! output.cellViewModelList.toBlocking().first()
        XCTAssertEqual(cellViewModelList?.count, 3)
    }

    func test_transform_triggerSearch_trackError() {
        // setup
        useCase.search_returnValue = Observable<SearchStatus>.error(TestError.test)
        let triggerSearchSubject = PublishSubject<Void>()
        let triggerSearchDriver = triggerSearchSubject.asDriverOnErrorJustComplete()
        let textDriver = BehaviorSubject<String>(value: "123").asDriverOnErrorJustComplete()
        let input = PhotoListViewModel.Input(
            text: textDriver,
            triggerSearch: triggerSearchDriver)
        let output = viewModel.transform(input: input)
        
        // test
        output.cellViewModelList.drive().disposed(by: disposeBag)
        output.error.drive().disposed(by: disposeBag)
        triggerSearchSubject.onNext(())

        // validate
        let error = try! output.error.toBlocking().first()
        XCTAssertNotNil(error)
    }

}

extension PhotoListViewModelTests {
    private func mockedPhotoList() -> [Photo] {
        return [
            Photo(
                id: "id1",
                owner: "owner1",
                secret: "secret1",
                server: "server1",
                farm: 1,
                title: "title1",
                isPublic: true,
                isFriend: false,
                isFamily: false),
            Photo(
                id: "id2",
                owner: "owner2",
                secret: "secret2",
                server: "server2",
                farm: 2,
                title: "title2",
                isPublic: false,
                isFriend: true,
                isFamily: false),
            Photo(
                id: "id3",
                owner: "owner3",
                secret: "secret3",
                server: "server3",
                farm: 3,
                title: "title3",
                isPublic: false,
                isFriend: false,
                isFamily: true)
        ]
    }
    
    private func mockedSeachDetails() -> SearchDetails {
        return SearchDetails(
            page: 1,
            totalPages: 2,
            itemsPerPage: 15,
            totalItems: 25,
            photoList: mockedPhotoList())
    }
    
    private func mockedSearchStatus() -> SearchStatus {
        return SearchStatus(
            status: .ok,
            code: nil,
            message: nil,
            searchDetails: mockedSeachDetails())
    }
}
