//
//  PhotosUseCaseMock.swift
//  PresentConnectionHomeworkTests
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

@testable import PresentConnectionHomework
import Domain
import RxSwift

class PhotosUseCaseMock: Domain.PhotosUseCase {
    var search_returnValue: Observable<SearchStatus> = Observable.empty()
    var search_called = false

    func search(text: String, maxUploadDate: TimeInterval, page: Int) -> Observable<SearchStatus> {
        search_called = true
        return search_returnValue
    }
}
