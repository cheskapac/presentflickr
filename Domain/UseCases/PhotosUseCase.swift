//
//  PhotosUseCase.swift
//  Domain
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation
import RxSwift

public protocol PhotosUseCase {
    func search(text: String, maxUploadDate: TimeInterval, page: Int) -> Observable<SearchStatus>
}
