//
//  UseCaseProvider.swift
//  Domain
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public protocol UseCaseProvider {
    func makePhotosUseCase() -> PhotosUseCase
}
