//
//  SearchError.swift
//  Domain
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public enum SearchError: Error {
    case unknown
    case status(code: Int, message: String)
}

extension SearchError: CustomDebugStringConvertible {
    public var debugDescription: String {
        switch self {
        case .unknown:
            return "Something unexpected happened"
        case let .status(code, message):
            return "\(message) - (\(code))"
        }
    }
}
