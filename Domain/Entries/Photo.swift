//
//  Photo.swift
//  Domain
//
//  Created by Paulius Cesekas on 09/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public struct Photo {
    public let id: String
    public let owner: String
    public let secret: String
    public let server: String
    public let farm: Int
    public let title: String?
    public let isPublic: Bool
    public let isFriend: Bool
    public let isFamily: Bool
    
    public init(
        id: String,
        owner: String,
        secret: String,
        server: String,
        farm: Int,
        title: String?,
        isPublic: Bool,
        isFriend: Bool,
        isFamily: Bool) {
            self.id = id
            self.owner = owner
            self.secret = secret
            self.server = server
            self.farm = farm
            self.title = title
            self.isPublic = isPublic
            self.isFriend = isFriend
            self.isFamily = isFamily
    }
}

extension Photo: Equatable {
    public static func == (lhs: Photo, rhs: Photo) -> Bool {
        return lhs.id == rhs.id &&
            lhs.owner == rhs.owner &&
            lhs.secret == rhs.secret &&
            lhs.server == rhs.server &&
            lhs.farm == rhs.farm &&
            lhs.title == rhs.title &&
            lhs.isPublic == rhs.isPublic &&
            lhs.isFriend == rhs.isFriend &&
            lhs.isFamily == rhs.isFamily
    }
}
