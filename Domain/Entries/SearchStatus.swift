//
//  SearchStatus.swift
//  Domain
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public struct SearchStatus {
    public enum Status: String {
        case ok
        case fail
    }
    
    public let status: Status
    public let code: Int?
    public let message: String?
    public let searchDetails: SearchDetails?
    
    public var error: Error? {
        guard let code = code,
            let message = message else {
                return nil
        }
        return SearchError.status(code: code, message: message)
    }
    
    public init(
        status: Status,
        code: Int?,
        message: String?,
        searchDetails: SearchDetails?) {
            self.status = status
            self.code = code
            self.message = message
            self.searchDetails = searchDetails
    }
}

extension SearchStatus: Equatable {
    public static func == (lhs: SearchStatus, rhs: SearchStatus) -> Bool {
        return lhs.status == rhs.status &&
            lhs.code == rhs.code &&
            lhs.message == rhs.message &&
            lhs.searchDetails == rhs.searchDetails
    }
}
