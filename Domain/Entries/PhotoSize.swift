//
//  PhotoSize.swift
//  Domain
//
//  Created by Paulius Cesekas on 11/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public enum PhotoSize: String {
    case smallSquare = "_s" // small square 75x75
    case largeSquare = "_q" // large square 150x150
    case thumbnail = "_t" // thumbnail, 100 on longest side
    case small240 = "_m" // small, 240 on longest side
    case small320 = "_n" // small, 320 on longest side
    case medium500 = "" // medium, 500 on longest side
    case medium640 = "_z" // medium, 640 on longest side
    case medium800 = "_c" // medium, 800 on longest side†
    case large1024 = "_b" // large, 1024 on longest side*
    case large1600 = "_h" // large, 1600 on longest side†
    case large2048 = "_k" // large, 2048 on longest side†
}
