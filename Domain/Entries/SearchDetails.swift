//
//  SearchDetails.swift
//  Domain
//
//  Created by Paulius Cesekas on 10/04/2018.
//  Copyright © 2018 PresentConnection. All rights reserved.
//

import Foundation

public struct SearchDetails {
    public let page: Int
    public let totalPages: Int
    public let itemsPerPage: Int
    public let totalItems: Int
    public let photoList: [Photo]
    
    public init(
        page: Int,
        totalPages: Int,
        itemsPerPage: Int,
        totalItems: Int,
        photoList: [Photo]) {
            self.page = page
            self.totalPages = totalPages
            self.itemsPerPage = itemsPerPage
            self.totalItems = totalItems
            self.photoList = photoList
    }
}

extension SearchDetails: Equatable {
    public static func == (lhs: SearchDetails, rhs: SearchDetails) -> Bool {
        return lhs.page == rhs.page &&
            lhs.totalPages == rhs.totalPages &&
            lhs.itemsPerPage == rhs.itemsPerPage &&
            lhs.totalItems == rhs.totalItems &&
            lhs.photoList == rhs.photoList
    }
}
